extends Control

func _physics_process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		visible = true
		get_tree().paused = true


func _on_Resume_pressed():
	get_tree().paused = false
	visible = false


func _on_Restart_pressed():
	_on_Resume_pressed()
	Global.reset()
	get_tree().reload_current_scene()


func _on_MainMenu_pressed():
	_on_Resume_pressed()
	Global.reset()
	get_tree().change_scene("res://scenes/MainMenu.tscn")
