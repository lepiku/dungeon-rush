extends MarginContainer


func _on_StartGame_pressed():
	#get_tree().change_scene("res://scenes/Intro.tscn")
	$Intro.visible = true


func _on_HowToPlay_pressed():
	Global.how_to_play = true
	#get_tree().change_scene("res://scenes/Intro.tscn")
	$Intro.visible = true


func _on_Quit_pressed():
	get_tree().quit()
