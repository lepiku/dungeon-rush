extends KinematicBody2D

signal hit

export var MOVEMENT_SPEED = 50
export var HEALTH_POINTS = 1
export var DAMAGE = 1
export var DISABLED = false

onready var player: Node2D = get_node("/root/World/Player")
onready var hp = HEALTH_POINTS

var hit_timer = 0

func _ready():
	$AnimationPlayer.play("walk")

func _physics_process(delta):
	if DISABLED:
		var dir = get_global_mouse_position() - global_position
		if dir.x < 0:
			$Sprite.set_flip_h(true)
		else:
			$Sprite.set_flip_h(false)
		$AnimationPlayer.play("idle")

	elif player != null:
		var dir = (player.global_position - global_position).normalized()
		if Global.za_warudo:
			dir = Vector2.ZERO
		var collision_info = move_and_collide(dir * MOVEMENT_SPEED * delta)

		if collision_info and hp > 0:
			var collider = collision_info.collider
			if collider.name == 'Player':
				collider.emit_signal('hit', DAMAGE)
			# queue_free()

		animation(dir)

	if hit_timer > 0:
		hit_timer -= delta
	elif hit_timer < 0:
		hit_timer = 0


func animation(direction):
	if Global.za_warudo:
		$AnimationPlayer.stop()
		return

	if direction.x < 0:
		$Sprite.set_flip_h(true)
	elif direction.x > 0:
		$Sprite.set_flip_h(false)

	if direction == Vector2.ZERO:
		$AnimationPlayer.play("idle")
	else:
		$AnimationPlayer.play("walk")


func _on_Enemy_hit():
	if hit_timer == 0:
		hit_timer = 0.4
		Global.htp_next(3)
		$SoundHit.play()
		hp -= 1

		# add energy if not froze
		if !Global.za_warudo:
			Global.energy = min(Global.energy + Global.INC_ENERGY, Global.MAX_ENERGY)

		if hp <= 0:
			visible = false
			collision_layer = 0
			collision_mask = 0
			yield(get_tree().create_timer(0.5), "timeout")
			queue_free()
