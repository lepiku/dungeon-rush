extends Control

signal next

const STAGES = 6

var stage = 1


func _ready():
	_on_HowToPlay_next(1)


func _on_Button_pressed():
	get_tree().paused = false
	visible = false
	stage += 1
	
	if stage > STAGES:
		Global.how_to_play = false


func _on_HowToPlay_next(num: int):
	if num >= stage:
		get_tree().paused = true
		visible = true
		var children = $MarginContainer/VBoxContainer/Labels.get_children()
		for child in children:
			if child.name == "Label" + str(stage):
				child.visible = true
			else:
				child.visible = false
