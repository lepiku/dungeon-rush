extends MarginContainer

var health_full = preload("res://assets/images/DungeonTilesetII/ui/ui_heart_full.png")
var health_half = preload("res://assets/images/DungeonTilesetII/ui/ui_heart_half.png")

var current_hp = INF

func _physics_process(delta):
	if current_hp != Global.hp:
		current_hp = Global.hp
		if Global.hp <= 1:
			$Top/HealthBar/FullHeart.set_visible(false)
			# TODO game over screen

		var full_health_size = Vector2(floor(Global.hp / 2) * 16, 16)
		$Top/HealthBar/FullHeart.set_custom_minimum_size(full_health_size)

		if Global.hp % 2 == 1:
			$Top/HealthBar/HalfHeart.set_visible(true)
		else:
			$Top/HealthBar/HalfHeart.set_visible(false)

	$Top/Ultimate/TextureProgress.value = Global.energy
