extends Node

const MAX_ENERGY = 100
const INC_ENERGY = 10

var hp = 6
var energy = 80
var za_warudo = false
var how_to_play = false

func reset():
	hp = 6
	za_warudo = false
	how_to_play = false

func htp_next(num: int):
	if Global.how_to_play:
		var htp = get_node("/root/World/Camera2D/CanvasLayer/HowToPlay")
		if htp != null:
			htp.emit_signal('next', num)
