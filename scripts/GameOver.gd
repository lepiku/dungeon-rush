extends MarginContainer

export var sceneName = "World"

func _physics_process(delta):
	if Global.hp > 0:
		self.visible = false
	else:
		self.visible = true

func _on_Restart_pressed():
	Global.reset()
	get_tree().reload_current_scene()


func _on_MainMenu_pressed():
	Global.reset()
	get_tree().change_scene("res://scenes/MainMenu.tscn")
