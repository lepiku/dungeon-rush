extends Node2D

const how_to_play = preload("res://scenes/HowToPlay.tscn")


func _ready():
	if Global.how_to_play:
		print('new htp')
		var htp = how_to_play.instance()
		$Camera2D/CanvasLayer.add_child(htp)


func _physics_process(delta):
	if Global.za_warudo and !$Camera2D/AudioStreamPlayer.stream_paused:
		$Camera2D/AudioStreamPlayer.stream_paused = true
	elif !Global.za_warudo and $Camera2D/AudioStreamPlayer.stream_paused:
		$Camera2D/AudioStreamPlayer.stream_paused = false


func _on_FinalRoom_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene("res://scenes/Finish.tscn")
