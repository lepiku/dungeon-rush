extends Area2D

export(int, 'right', 'down', 'left', 'up') var ARROW_DIRECTION
export(int, 0, 1) var ARROW_SIDE

const spawn_margin = 16

onready var camera = $"/root/World/Camera2D"

var rng = RandomNumberGenerator.new()
var camera_moved = false

func _ready():
	rng.randomize()
	var arrow_main = [Vector2(448, 136), Vector2(240, 224), Vector2(32, 136), Vector2(240, 48)]
	var arrow_offset = [Vector2(0, 72), Vector2(192, 0)]
	var sig
	if ARROW_SIDE == 0:
		sig = -1
	else:
		sig = 1
	$Next.position = arrow_main[ARROW_DIRECTION] + arrow_offset[ARROW_DIRECTION % 2] * sig
	$Next.rotation_degrees = 90 * ARROW_DIRECTION

func _physics_process(delta):
	var children = $Enemies.get_children()
	
	# if all enemy killed
	if len(children) == 0:
		$Next.visible = true
		Global.htp_next(4)
		
		if name == 'Room2':
			Global.htp_next(6)
	else:
		$Next.visible = false


func _on_Next_body_entered(body):
	if $Next.visible and body.name == 'Player' and !camera_moved:
		camera_moved = true
		if ARROW_DIRECTION == 0:
			camera.position.x += 480
		elif ARROW_DIRECTION == 1:
			camera.position.y += 270
		elif ARROW_DIRECTION == 2:
			camera.position.x -= 480
		elif ARROW_DIRECTION == 3:
			camera.position.y -= 270


func _on_BossRoom_body_entered(body):
	if body.name == 'Player':
		$"Enemies/BossOgre".DISABLED = false
