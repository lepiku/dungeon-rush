extends Area2D

export(int) var NUMBER_OF_ENEMIS
export(int, 'right', 'down', 'left', 'up') var ARROW_DIRECTION
export(int, 0, 1) var ARROW_SIDE

const enemy_tiny_zombie = preload("res://scenes/enemies/TinyZombie.tscn")
const enemy_orc_warrior = preload("res://scenes/enemies/OrcWarrior.tscn")
const enemy_chort = preload("res://scenes/enemies/Chort.tscn")
const enemy_choice = [enemy_tiny_zombie, enemy_orc_warrior, enemy_chort]
const spawn_margin = 16

onready var camera = $"/root/World/Camera2D"

var rng = RandomNumberGenerator.new()
var camera_moved = false

func _ready():
	rng.randomize()
	var arrow_main = [Vector2(448, 136), Vector2(240, 224), Vector2(32, 136), Vector2(240, 48)]
	var arrow_offset = [Vector2(0, 72), Vector2(192, 0)]
	var sig
	if ARROW_SIDE == 0:
		sig = -1
	else:
		sig = 1
	$Next.position = arrow_main[ARROW_DIRECTION] + arrow_offset[ARROW_DIRECTION % 2] * sig
	$Next.rotation_degrees = 90 * ARROW_DIRECTION
	spawn_enemy()

func _physics_process(delta):
	var children = $Enemies.get_children()
	
	# if all enemy killed
	if len(children) == 0:
		$Next.visible = true
		Global.htp_next(4)
		
		if name == 'Room2':
			Global.htp_next(6)
	else:
		$Next.visible = false
	

func spawn_enemy():
	var area_position = $CollisionShape2D.position
	var area_size = $CollisionShape2D.shape.extents
	
	for i in NUMBER_OF_ENEMIS:
		var x = rng.randf_range(area_position.x - area_size.x + spawn_margin, area_position.x + area_size.x - spawn_margin)
		var y = rng.randf_range(area_position.y - area_size.y + spawn_margin, area_position.y + area_size.y - spawn_margin)
		var enemy_num = rng.randi_range(0, len(enemy_choice) - 1)
		var enemy_type: PackedScene = enemy_choice[enemy_num]
		var enemy = enemy_type.instance()
		enemy.DISABLED = true
		
		enemy.position = Vector2(x, y)
		$Enemies.add_child(enemy)


func _on_Room_body_entered(body):
	if body.name == 'Player':
		for enemy in $Enemies.get_children():
			enemy.DISABLED = false
			print('disable ', enemy, enemy.name)
		if name == 'Room2':
			Global.htp_next(5)


func _on_Next_body_entered(body):
	if $Next.visible and body.name == 'Player' and !camera_moved:
		camera_moved = true
		if ARROW_DIRECTION == 0:
			camera.position.x += 480
		elif ARROW_DIRECTION == 1:
			camera.position.y += 270
		elif ARROW_DIRECTION == 2:
			camera.position.x -= 480
		elif ARROW_DIRECTION == 3:
			camera.position.y -= 270
		
