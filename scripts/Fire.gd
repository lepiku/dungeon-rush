extends Node2D


func _physics_process(delta):
	if Global.za_warudo:
		$AnimationPlayer.stop()
	else:
		$AnimationPlayer.play("play")


func _on_Fire_body_entered(body: Node2D):
	if body.name == 'Player':
		body.emit_signal('hit', 1)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
