extends Control

var step = 1


func _on_LinkButton_pressed():
	get_node("CenterContainer/Label" + str(step)).visible = false
	step += 1
	
	if step <= 3:
		get_node("CenterContainer/Label" + str(step)).visible = true
	else:
		get_tree().change_scene("res://scenes/World.tscn")
	
	if step == 3:
		$MarginContainer/LinkButton.text = "Play"

