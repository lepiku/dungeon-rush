extends KinematicBody2D

signal hit

export var disabled = false

const MOVEMENT_SPEED = 1500
const FRICTION = 10
const MOVE_TIME = 0.5
const INVIS_TIME = 2

const fire = preload("res://scenes/Fire.tscn")

var velocity = Vector2()
var move_timer = 0
var invis_timer = 0


func get_input():
	var mouse_position = get_global_mouse_position()
	$PlayerAim.look_at(mouse_position)
	
	if Input.is_action_just_pressed("click") and move_timer == 0 and !disabled:
		move_timer = MOVE_TIME
		var dist = mouse_position - global_position
		var move_dist = min(dist.length() * FRICTION, MOVEMENT_SPEED)
		velocity = dist.normalized() * move_dist
		add_fire()
		Global.htp_next(2)

	# animation
	if mouse_position.x < self.position.x:
		$AnimatedSprite.flip_h = true
		$PlayerAim/Sword.flip_h = false
	else:
		$AnimatedSprite.flip_h = false
		$PlayerAim/Sword.flip_h = true


func set_hitbox_visible():
	# moving
	if move_timer > 0:
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(2, false)
		set_collision_mask_bit(3, false)

		$PlayerAim/Sword.self_modulate.a = 1
		$PlayerAim/SwordHitbox.set_collision_layer_bit(4, true)
		$PlayerAim/SwordHitbox.set_collision_mask_bit(2, true)
		$PlayerAim/SwordHitbox.set_collision_mask_bit(3, true)

	# still
	else:
		set_collision_layer_bit(1, true)
		set_collision_mask_bit(2, true)
		set_collision_mask_bit(3, true)

		$PlayerAim/Sword.self_modulate.a = 0.5
		$PlayerAim/SwordHitbox.set_collision_layer_bit(4, false)
		$PlayerAim/SwordHitbox.set_collision_mask_bit(2, false)
		$PlayerAim/SwordHitbox.set_collision_mask_bit(3, false)
		
	if invis_timer > 0:
		$AnimatedSprite.self_modulate.a = 0.5
	else:
		$AnimatedSprite.self_modulate.a = 1

	if disabled:
		$PlayerAim/Sword.self_modulate.a = 1


func add_fire():
	var new_fire = fire.instance()
	new_fire.global_position = global_position
	get_node('..').add_child(new_fire)


func _ready():
	$AnimatedSprite.play('idle')


func _physics_process(delta):
	get_input()
	set_hitbox_visible()
	velocity = move_and_slide(velocity)
	velocity = lerp(velocity, Vector2.ZERO, FRICTION * delta)


func _process(delta):
	if move_timer > 0:
		move_timer -= delta
	elif move_timer < 0:
		move_timer = 0

	if invis_timer > 0:
		invis_timer -= delta
	elif invis_timer < 0:
		invis_timer = 0


func _on_SwordHitbox_body_entered(body: KinematicBody2D):
	if $PlayerAim/SwordHitbox.get_collision_layer_bit(4):
		body.emit_signal('hit')


func _on_Player_hit(damage: int):
	if invis_timer == 0:
		Global.hp -= damage
		if Global.hp <= 0:
			queue_free()
		invis_timer = INVIS_TIME
