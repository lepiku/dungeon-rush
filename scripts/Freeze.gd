extends Position2D

const MAX_SIZE = 1000
const TIME_BEGIN = 1
const TIME_FREEZE = 3
const TIME_CONTINUE = 1

var size = 0
var time = 0
var resumed = false


func _draw():
	draw_circle(position, size, Color(1, 0, 1, 0.4))


func _physics_process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		print('energy ', Global.energy)
	if Input.is_action_just_pressed("ui_accept") and time == 0 and !$"..".disabled and Global.energy == Global.MAX_ENERGY:
		Global.za_warudo = true
		resumed = false
		$"../StopSound".play()

	# increase circle size
	if Global.za_warudo and time < TIME_BEGIN:
		size = time / TIME_BEGIN * MAX_SIZE
		update()
	
	# decrease circle size
	if Global.za_warudo and time > TIME_BEGIN + TIME_FREEZE:
		if !resumed:
			$"../ResumeSound".play()
			resumed = true

		size = (TIME_BEGIN + TIME_FREEZE + TIME_CONTINUE - time) \
				/ TIME_CONTINUE * MAX_SIZE
		update()


func _process(delta):
	if Global.za_warudo:
		time += delta
		Global.energy = Global.MAX_ENERGY - (time / (TIME_BEGIN + TIME_FREEZE + TIME_CONTINUE)) * Global.MAX_ENERGY

		if time >= TIME_BEGIN + TIME_FREEZE + TIME_CONTINUE:
			time = 0
			size = 0
			update()
			Global.za_warudo = false
			Global.energy = 0
