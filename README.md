# Dungeon Rush

A game about a knight saving the princess who's stuck inside a dungeon.
Made by Muhammad Oktoluqman Fakhrianto for GameDev assignment at Computer Science at University of Indonesia.

Itch.io link: [https://lepiku.itch.io/dungeon-rush](https://lepiku.itch.io/dungeon-rush)
